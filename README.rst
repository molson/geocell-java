==============
 Geocell-Java
==============

Geocell-Java is based on the GeoModel approach described by the `Geospatial
Queries with Google App Engine using GeoModel`_ article. The source code for
that project, in Python, is available at the `GeoModel repository`_. Like
GeoModel and Geohash_, Geocell-Java is a quad tree-like algorithm for
geospatial hashing. While a quad-tree divides the space into quadrants, these
algorithms divide the space into halves at each step. The Geocell-Java library
can accept and output the base 16 strings used by the Python implementation of
GeoModel, but has a broader range of formats available for use. The supported
formats are described in Formats_. To understand the format trade-offs, make
sure to read the section on Resolution_.


Getting Started
===============

The best place to get started is with the `Geospatial Queries`_ article linked
above. It explains the general idea behind geocells and how to make use of them
in greater detail than will be described below. In this document, we will focus
on the differences and how to use the library itself. The Usage_ section
explains the most commonly needed functions in the library and how to call
them. If you are not using GWT, you will only need the functions in
``edu.caltech.csn.geocell``, and can ignore all of the other source files. The
code in ``edu.caltech.csn.gwt`` provides example bindings for GWT and, powering
the `Geocell Map`_ website, shows how the library can be used to retrieve
geocell sets for the visible area of a user's map.


Determining the Geocell
=======================

The GeoModel algorithm relies on a choice of cells from a 4x4 grid of options.
This is because each character in a GeoModel string represents 4 different
choices in a decision tree. Geocell-Java allows each choice to be specified
independently, enabling any number of choices to be encoded as a geocell. The
number of choices in the decision string is referred to as the resolution_. To
be able to represent arbitrary resolutions, geocell objects are represented
internally as 64-bit longs. The first 58 bits are reserved for decision data,
while the last 6 bits are reserved for storing the resolution information. The
bits are individually manipulated to represent successive choices of direction.

Like GeoModel, the algorithm begins with the bounds of the world, and each bit
represents whether the point being described lies west or east of the mean
longitude, for odd bits, or north or south of the central latitude, for even
bits (in GeoModel, the bit order is reversed). For odd bits, if the point lies
east of the mean longitude, the longitude bit is set to 1; otherwise, it is set
to 0. For even bits, if the points lies north of the mean latitude, the
latitude bit is set to 1; otherwise, it is set to 0. Once any choice is made,
the bounds described by the geocell are updated.

That is, the first bit identifies if the point is in the eastern or western
hemisphere, while the second bit identifies if the point is in the northern or
southern hemisphere. This continues until the desired number of bits have been
decided. To work a small example, consider the point (34.14° N,
118.12° W). The point lies in the northwest quadrant, yielding a bit pair of
``01`` for a resolution of 2. Iterating through the algorithm yields the
following bits for a resolution of 28::

    0100110110100000010000000101

The below image illustrates how the algorithm successively refines bounds when
working with the point in red. The illustration shows successive refinement
with a resolution step size of 2.

.. image:: http://cdn.bitbucket.org/molson/geocell-java/downloads/geocell.png
   :alt: Successive refinement of geocell boundaries.


Resolution
==========

The resolution of a geocell refers to the depth of the decision tree used to
represent the location. It is also equivalent to the number of bits of
meaningful information. The bits are considered to be numbered left to right,
beginning with 1. Odd numbered bits, including the first bit, represent
a choice of longitude and are called longitude bits; even numbered bits
represent a choice of latitude and are called latitude bits.

GeoModel bit order is reversed, beginning with a latitude choice. The
resolution choice is limited by the direction expression of resolution as
a function of string length. Because the resolution of a GeoModel string is
defined by the length of the string, and each character represents 4 bits, then
a GeoModel string is only capable of representing resolutions that are
multiples of 4. In geocell parlance, a length one GeoModel string is resolution
4, a length 2 GeoModel string is resolution 8, and so on.

As described above, 64-bit longs are used internally to represent geocells.
With six bits reserved to store the resolution, the library supports a maximum
possible resolution of 58. Using a long is very efficient for manipulation, but
can be wasteful for storage if the expense of 8 bytes to store a single
resolution could be considered problematic. The size trade-offs between formats
are described in Formats_ and more directly in the `comparison of storage size
at different resolutions`_ spreadsheet. In Usage_, converting the algorithm
results to storage formats other than long is described.

To visualize the size of different resolutions at different points on earth,
you can visit the `Geocell Map`_ website. The "auto-select resolution" option,
which is on by default, calls the ``getQueryCells`` function of
``GeocellLibrary``. This is useful for visualizing the results of the current
cost and merge function embedded in ``GeocellLibrary``, but makes it more
difficult to tell what resolution objects are being visualized.

Deselecting the "auto-select resolution" option will enable the selection of
any resolution. Selecting a specific resolution will tessellate geocell objects
of that resolution across the visible area. In some cases, if such
a visualization requires too many objects, the program may refuse to attempt to
draw that many polygons or, with some browsers, may fail in the attempt to do
so. Choose larger resolutions or zoom in when choosing smaller resolutions to
guarantee that visualization is possible.

Here and in the documentation, we refer to *larger* or *higher* resolutions as
resolutions that occupy more physical space; this implies that the resolution
ordinal is lower, however. This convention is established to make the relative
comparison between geocells be related to the physical size of geocells at that
resolution. That is, resolution 1 is the largest possible resolution,
representing only whether a point lies in the eastern or western hemisphere,
while a resolution 58 geocell, the smallest representable resolution, specifies
a space which is about ``22 cm^2``, or maybe four postage stamps.


Aspect ratio
============

The aspect ratio of a geocell is affected by two factors:

- The number of latitude versus longitude bits used.
- The place on Earth where a geocell is rendered.

The latter effect is a result of the decreasing distance between lines of
longitude as one moves away from the equator. Thus, while the distance between
latitudes remains constant, the decreasing distance between longitudes as one
moves away from the equator changes the aspect ratio of the resulting geocell.

In the following table, you can see how the size and aspect ratio of the
geocells changes as a result of the two factors above for different places on
earth. We define "height" as the distance between latitudes and "width" as the
distance between longitudes, then give their ratio to show the changes.

+-------------------------+---------+---------+--------+-----------+
+                         + Jakarta + Caltech + London + Reykjavik +
+=========================+=========+=========+========+===========+
| Area / Jakarta Area     |   1.00  |  0.83   |  0.63  |   0.44    |
+-------------------------+---------+---------+--------+-----------+
| Height / Width with an  |         |         |        |           |
| equal number of         |   1.99  |  1.66   |  1.24  |   0.87    |
| latitude/longitude bits |         |         |        |           |
+-------------------------+---------+---------+--------+-----------+
| Height / Width with one |         |         |        |           |
| more longitude bit than |   0.99  |  0.83   |  0.62  |   0.44    |
| latitude bits           |         |         |        |           |
+-------------------------+---------+---------+--------+-----------+

Thus, you can see that odd numbered resolutions are perfect squares at the
equator, while even numbered resolutions approach perfect squares as one moves
farther and farther away from the equator. Additionally, the amount of land
that a geocell covers decreases as one moves away from the equator. Whether or
not the shape and size of the geocell changing is important depends on your
application, but, if it does, and your application deals with a local area, it
is wise to investigate the preferred shape and size for the area on Earth that
you will be hashing latitude/longitude values from.

Because changing the parity of the resolution used for a geocell also changes
its aspect ratio, it is often considered desirable to use
a ``RESOLUTION_STEP_SIZE`` value of a multiple of 2. The step size is discussed
in Usage_.


Formats
=======

Internally, all geocell objects are represented as 64-bit longs. Geocell-Java
can output, and translate between, the following formats using converter
objects:

long : 64 bits
    Used internally. The first 58 bits store the directional data, while the
    last 6 bits store the resolution. Always requires 8 bytes and supports
    resolutions up to 58.

int : 32 bits
    The first 27 bits store the directional data, while the last 5 bits store
    the resolution. Requires only 4 bytes, but only supports resolutions up to
    27.

Geostring
    Uses URL-safe base64-encoded strings. The first character encodes the
    resolution, while the remaining characters describe the directional data.
    Requires ``Math.ceiling(resolution / 6) + 1`` bytes and only supports
    resolutions up to 58.

GeoModel_
    Uses base 16 strings for encoding, with the length of the string describing
    the resolution. Requires ``resolution * 4`` bytes. Supports any resolution
    that is a multiple of 4. Note: internal converter can support arbitrary
    resolutions as GeoModel strings by appending directional components to the
    end of a standard GeoModel string, but this is not standard and would not
    be supported outside of this library.

Geohash_
    Uses specialized for readability base 32 encoding, with the length of the
    string describing the resolution. Requires ``resolution * 4/5`` bytes.
    Supports any resolution that is a multiple of 5. The odd resolution step
    size implies alternating aspect ratio between resolution steps.

The storage size trade-offs are explicitly laid out in the `comparison of
storage size at different resolutions`_ spreadsheet up to the maximum
resolution supported by Geocell-Java.


Usage
=====

The primary class of Geocell-Java is ``GeocellLibrary``. It is suffixed with the
word ``Library`` because it is not itself an object, but it is rather
a collection of functions for manipulating longs. For the purpose of creating
and manipulating long objects, no other class is directly necessary.
``GeocellLibrary`` makes use of ``GeocellContainer``, ``Point``, and ``Bounds``
internally for state maintenance. If you are manipulating objects and intend to
represent their bounds, you will want to call the constructors in
``GeocellContainer`` directly when creating a geocell.

There is currently no way to change the resolutions returned by functions like
``getGeocellSet`` other than to modify the constants in GeocellLibrary. If you
are not using the set returning functions, you needn't bother modifying those
constants, but the ancestry functions also use ``RESOLUTION_STEP_SIZE`` to
determine how many resolutions to step over when getting child or parent
objects.

If you would like to utilize other formats for storage or display, you will want
to access the classes in ``edu.caltech.csn.geocell.converter``, which provide
static functions for translation to and from their implemented formats, as well
as implementing the interface (``edu.caltech.csn.geocell.converter.Converter``)
necessary to get ``GeocellLibrary`` to output that format for functions
returning sets.

The functions in ``GeocellLibrary`` are mostly well documented, but we will
highlight the most important ones here:

``getGeocell``
    Overridden to support ``Point`` objects and pairs of ``double`` objects as
    input. You will not normally want to call this function without specifying a
    resolution. A resolution 58 geocell is a reasonable analogue for a point, if
    you do not want to store pairs of latitude/longitude, but is not useful for
    aggregation or queries.

``get{East|West|North|South}``
    Given a long geocell, these functions return the long geocell at the same
    resolution one grid space east, west, north, or south of the provided
    geocell. Useful for constructing grids of geocells when the existing
    functions are inadequate, for instance if the 8 geocells surrounding
    a particular geocell are desired.

``getGeocellSet``
    Returns all of the geocells in the *useful resolution set*. That is the set
    of resolutions specified by the following constants defined in
    ``GeocellLibrary``: ``MIN_USEFUL_RESOLUTION``, ``MAX_USEFUL_RESOLUTION``,
    and ``RESOLUTION_STEP_SIZE``. The possible resolutions returned by
    ``getQueryCells`` and ``getGeocellSet`` are defined by the resolutions in
    the loop::

        for (int i = MIN_USEFUL_RESOLUTION; i <= MAX_USEFUL_RESOLUTION;
            i += RESOLUTION_STEP_SIZE) {}

``getQueryCells``
    Returns a set of cells that cover a given bounded region. ``getQueryCells``
    does not necessarily return one resolution; it can return any resolution in
    the useful resolution set (see ``getGeocellSet``). It performs merging of
    smaller geocells into larger geocells that are also members of the useful
    resolution set, as well as shrinking of corners into smaller geocells if
    possible. See the code for full details.

    To visualize the merging and shrinking process, visit the `Geocell Map`_ and
    leave on the "auto-select resolution" option. Press the "Freeze Cells"
    button to have the program draw a blue rectangle through the space where the
    map viewport previously was, while you can pan around, zoom in, and zoom out
    to inspect what set of geocells was used to cover the viewable area. Here is
    an `example frozen region with merged cells and shrunk corners`_.

    There is a cost function embedded in ``getQueryCells`` that strongly favors
    smaller numbers of cells. You will observe the merging of cells more readily
    when viewing larger areas of land, which is the only time the cost function
    will begin to favor larger numbers of cells. This cost function should, and
    ultimately will be, abstracted out of GeocellLibrary. For the time begin,
    the ``getQueryCells`` function can be used as a template for designing
    a similar algorithm with a different set of priorities.

``fixedGrid``
    Like ``getQueryCells``, this will return a set of cells that cover a given
    bounded region. This function, however, requires the resolution to be
    specified. It will return the smallest set of cells that covers the given
    region at the specified resolution.


Examples
========

The below sections cover the most commonly used functions for three specific use
cases.

Finding objects by equality queries
-----------------------------------

This is the most straightforward use case, and the one covered by the
`Geospatial Queries`_ article. To accomplish this, you must either know
a priori what resolution you intend to perform queries at, or store a set of
resolution objects and query across the set of resolution objects.

When indexing an object with location data for future retrieval, you can store
a set of resolutions as Geostring objects like this::

    geostringSet = GeocellLibrary.getGeocellSet(
            latitude, longitude, new GeostringConv());

Then, in a client side map, to get the cells to query, you would call::

    geocells = GeocellLibrary.getQueryCells(
        bounds.getSouthWest().getLatitude(),
        bounds.getSouthWest().getLongitude(),
        bounds.getNorthEast().getLatitude(),
        bounds.getNorthEast().getLongitude(),
        new GeostringConv());

Passing the geocell set returned above to a server side handler enables you to
query for matching objects like so::

    Query query = pm.newQuery(GeospatialObject.class);
    query.setFilter("geostringSet.contains(geostrings)");
    query.declareImports("import java.util.Collection");
    query.declareParameters("Collection geostrings");
    List<Object> queryResults = query.execute(geostrings);


Aggregating geospatial data
---------------------------

Incoming geospatial data can be hashed into bins using geocells, possibly
crossed with time objects for temporal streams. This can be used to do things
like geographically sharded counters (see `Counter Sharding`_), or to aggregate
information over a region rather than storing information as individual points.
When aggregating information over a geocell bounded area, you will typically
want to fix the resolution, or resolutions, used for aggregation in a constant
somewhere. Then, you would get the appropriate hash object for a geospatial
object by calling the ``getGeocell`` function like so::

    long hashKey = GeocellLibrary.getGeocell(
            geospatialObj.getPoint(), AGGREGATION_RESOLUTION);
    MemcacheService ms = MemcacheServiceFactory.getMemcacheService();
    Long numObjs = ms.increment(hashKey, 1, 0L);


Obscuring the source of geospatial data
---------------------------------------

A typical approach to obscuring the true location of an event is to fuzz the
location with an offset. If done enough times randomly, this is sufficient to
reveal the underlying location. Another solution is to abstract the location
into a geocell. That is, rather than representing a fuzzed point on the map,
you represent, possibly in aggregate, the information as a geocell bound. This
is exactly how the `CSN Client Map`_ represents client locations in the
Community Seismic Network.

The location is known only at the resolution of the chosen geocell. Nothing can
be determined about where within that geocell the true location is unless so
little exists in that geocell that the source can be pinpointed by visual
inspection. In such a situation, fuzzing the location would not help either,
and choosing a higher resolution geocell for abstraction may be appropriate.

To employ this strategy, you generally want to fix the smallest (highest
ordinal) resolution you are willing to utilize and then adapt the display
resolution to the current size of the map. This can be accomplished by setting
the display resolution as a function of the zoom level. For the Google Maps API,
a rough guide is::

    displayResolution = Math.min(
        (zoomLevel + 4) * 2 - 1, MAX_DISPLAY_RESOLUTION);

The ``- 1`` in the above calculation is for the use of odd parity display
resolutions. For even parity display resolutions, that figure can be dropped.
Whenever the ``displayResolution`` figure changes as a result of the map zoom
level changing, simply take the original geocell object and call
``decreaseResolution`` on it; then merge results which now occupy the same
geocell. See the GWT method ``GwtGeocell.getPolygon()`` for an example of how
to get a polygon that represents a given geocell object.


Caution for App Engine users
============================

Note that App Engine's pricing changes last year made indexes on frequently
written geospatial data costly. That is because the new datastore charges
include a fee per write operation, which includes index writes. If you are
using App Engine with a constantly changing data set, such as a stream of
geospatial information, you will want to limit the number of indexes created.
One simple way to do that is to limit the number of resolutions stored. This
also limits your query flexibility, but will help reduce your index write
costs. To observe the number of write operations per object with App Engine,
use the App Engine dev server locally. See the `Managing Your App's Resource
Usage`_ article along with the `Understanding Write Costs`_ article for more
information.


.. GeoModel
.. _Geospatial Queries with Google App Engine using GeoModel:
.. _Geospatial Queries: https://developers.google.com/maps/articles/geospatial
.. _GeoModel repository:
.. _GeoModel: http://code.google.com/p/geomodel/

.. Comparison
.. _Geohash: http://en.wikipedia.org/wiki/Geohash
.. _comparison of storage size at different resolutions: http://goo.gl/YNnG1

.. Geocell
.. _Geocell Map: http://geocell-map.appspot.com
.. _example frozen region with merged cells and shrunk corners:
    http://geocell-map.appspot.com/#mc=37.09024,-125.15625
    &mz=5&fr=27.508271,-136.362305+47.23449,-112.148437&as=1
.. _CSN Client Map: http://map.communityseismicnetwork.org/#clients

.. App Engine
.. _Managing Your App's Resource Usage:
    https://developers.google.com/appengine/articles/managing-resources
.. _Understanding Write Costs:
    https://developers.google.com/appengine/docs/java/datastore/entities
    #Understanding_Write_Costs
.. _Counter Sharding:
    https://developers.google.com/appengine/articles/sharding_counters
