package edu.caltech.csn.geocell.converter;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.GeocellLibrary;

/**
 * Convert to/from binary 0/1 or [N/S,E/W] representations.
 * <p><ul>
 * <li>Resolution min: 1
 * <li>Resolution max: 58
 * <li>Step size: 1
 * </ul><p>
 * Decision strings and binary strings are the same thing, although decision
 * strings make it more obvious what is going on.
 *
 * @author Michael Olson <michael.olson@gmail.com>
 * @see http://code.google.com/p/geomodel/
 */
public class BinaryStringConv implements Converter<String> {

	private static final boolean USE_DECISION_STRING = true;

	/**
	 * Function used mainly for debugging which describes the decision steps.
	 * <p>
	 * Outputs the direction choices made at each point in the search tree.
	 * This is the same as the binary string except that 1 is
	 * represented as E or N depending on the resolution's parity and 0 is
	 * represented as W or S depending on the resolution's parity.
	 *
	 * @param geocell geocell to convert to a decision string
	 * @return string showing each step made in the quaternary search tree
	 */
	public static String toDecisionString(final long geocell) {
		final StringBuilder result = new StringBuilder();
		final int resolution = GeocellLibrary.getResolution(geocell);
		for (int i = 1; i <= resolution; i++) {
			result.append(gridLoc(GeocellLibrary.getBit(geocell, i), i));
		}
		return result.toString();
	}

	/**
	 * Function to indicate the directional choice made in the decision tree.
	 *
	 * @param bit the bit to retrieve the corresponding cardinal direction for
	 * @return NW, NE, SE, or SW as appropriate
	 */
	public static String gridLoc(final long bit, final int resolution) {
		if ((resolution & 1) == 1) {
			if (bit == 1) {
				return "E";
			} else {
				return "W";
			}
		} else {
			if (bit == 1) {
				return "N";
			} else {
				return "S";
			}
		}
	}

	/**
	 * Returns a zero-padded binary string that shows the significant zeros.
	 * <p>
	 * Significant zeros are those that represent a decision made.
	 * Long.toBinaryString does not preserve leading zeros.  This function uses
	 * the geocell's resolution to determine which leading zeros should be
	 * preserved and ensures that they are prepended to the resulting binary
	 * string.
	 * <p>
	 * This does not include the resolution bits of the geocell, only the
	 * decision bits.
	 *
	 * @param geocell the geocell to convert to a binary string.
	 * @return a significant-zero padded binary string
	 */
	public static String toBinaryString(final long geocell) {
		final int resolution = GeocellLibrary.getResolution(geocell);
		final String binary = Long.toBinaryString(geocell >>>
						GeocellLibrary.shiftDistance(resolution));
		final StringBuilder zeros = new StringBuilder();
		for (int i = binary.length(); i < resolution; i++) {
			zeros.append("0");
		}
		zeros.append(binary);
		return zeros.toString();
	}

	/**
	 * Returns the raw hex string which represents a geocell.
	 * <p>
	 * This is simply a more compact representation of the binary
	 * representation of the geocell.  It includes the resolution bits as
	 * well as the decision bits.
	 *
	 * @param geocell the unshifted geocell to convert to a hex string
	 * @return the hex string of the specified geocell
	 */
	public static String toRawHexString(final long geocell) {
		final String hex = Long.toHexString(geocell);
		final StringBuilder result = new StringBuilder();
		result.append("0x");
		// CHECKSTYLE IGNORE MagicNumber FOR NEXT 1 LINE.
		for (int i = hex.length(); i < 16; i++) {
			result.append("0");
		}
		result.append(hex);
		return result.toString();
	}

	@Override
	public String fromGeocellContainer(final GeocellContainer geocellContainer) {
		if (USE_DECISION_STRING) {
			return toDecisionString(geocellContainer.getGeocell());
		} else {
			return toBinaryString(geocellContainer.getGeocell());
		}
	}

	@Override
	public String fromGeocell(final long geocell) {
		if (USE_DECISION_STRING) {
			return toDecisionString(geocell);
		} else {
			return toBinaryString(geocell);
		}
	}

	@Override
	public long toGeocell(final String convertedGeocell) {
		if (USE_DECISION_STRING) {
			String result = convertedGeocell.replace('N', '1');
			result = convertedGeocell.replace('S', '0');
			result = convertedGeocell.replace('E', '1');
			result = convertedGeocell.replace('W', '0');
			return Long.valueOf(result, 2);
		} else {
			return Long.valueOf(convertedGeocell, 2);
		}
	}
}
