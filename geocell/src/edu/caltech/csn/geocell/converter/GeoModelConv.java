package edu.caltech.csn.geocell.converter;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.GeocellLibrary;

/**
 * Convert to/from GeoModel format; limits possible resolutions.
 * <p><ul>
 * <li>Resolution min: 4
 * <li>Resolution max: 56
 * <li>Step size: 4
 * </ul><p>
 * Note: the specified maximum resolution is imposed by the internal
 * representation as a long. The Geomodel format can natively support any
 * resolution that is a multiple of 4.
 *
 * @author Michael Olson <michael.olson@gmail.com>
 * @see http://code.google.com/p/geomodel/
 */
public class GeoModelConv implements Converter<String> {

	private static final int GEOMODEL_BIT_WIDTH = 4;
	private static final int GEOMODEL_BASE_FORMAT = 16;
	private static final int MAX_CONVERTIBLE_LENGTH = 14;
	private static final int MAX_CONVERTIBLE_EXTENDED = 17;

	/**
	 * Outputs geomodel style strings.
	 *
	 * Given a resolution bit depth of 2, each geomodel character is equal to
	 * 2 resolution steps.  That means only resolutions that are divisible by
	 * 4 can be converted to geomodel strings in a 1:1 correlation.  Odd
	 * resolutions are converted by appending ".{dir}" where dir is equal to
	 * the last decision step: northwest (nw), southwest (sw), northeast (ne),
	 * or southeast (se), which is not encoded by the geomodel string.
	 *
	 * @param geocell geocell to convert to a geomodel style string
	 * @return string where each character embeds 4 steps in the decision tree
	 * @see http://code.google.com/p/geomodel/
	 */
	public static String toGeoModelString(final long geocell) {
		final int resolution = GeocellLibrary.getResolution(geocell);
		final long source;
		if (resolution % 2 == 0) {
			source = bitTwiddler(geocell);
		} else {
			source = geocell;
		}
		final int rounded = resolution / GEOMODEL_BIT_WIDTH;

		// Geomodel strings use hex characters that comprise 4 bits.  The first
		// two bits of a geocell long are empty, so to get the same hex bits
		// we must shift left or right by 2 bits, derive the hex
		// representation, and then take the resolution appropriate substring
		// of the resulting string.
		if (resolution % GEOMODEL_BIT_WIDTH == 0) {
			return Long.toHexString(source).substring(0, rounded);
		} else {
			final StringBuffer result = new StringBuffer();
			result.append(Long.toHexString(source).substring(0, rounded)).append('.');
			for (int i = rounded * GEOMODEL_BIT_WIDTH + 1; i <= resolution; i++) {
				result.append(BinaryStringConv.gridLoc(GeocellLibrary.getBit(source, i), i));
			}
			return result.toString();
		}
	}

	/**
	 * Method to construct a geocell from a base 16 string representation of
	 * a geocell.
	 *
	 * Note: the 2 magic number is really the number of bits in a single
	 * character, 4, divided by the bit width, 2.  That is, it represents the
	 * number of resolution steps encoded in a single character.
	 *
	 * The longest supported Geomodel string is 14 (assuming no extension;
	 * with extension, it's 17: 13 + 1 for the period + 3 for the extension).
	 * This function will truncate longer strings to 14 characters long if
	 * necessary.
	 *
	 * @param geostring geomodel'esque string representation of the geocell
	 * @return the long representing the geocell
	 * @see http://code.google.com/p/geomodel/
	 */
	public static long fromGeoModelString(final String geostring) {
		final int resolution;
		long geocell;

		if (geostring.contains(".") && geostring.length() <= MAX_CONVERTIBLE_EXTENDED) {
			final int dotAt = geostring.indexOf('.');
			final int extLen = geostring.length() - dotAt - 1;
			resolution = dotAt * GEOMODEL_BIT_WIDTH + extLen;
			geocell =
				Long.parseLong(geostring.substring(0, dotAt), GEOMODEL_BASE_FORMAT);

			for (int i = 0; i < extLen; i++) {
				geocell = geocell << 1;
				final char chr = geostring.charAt(dotAt + i + 1);
				if (chr == 'E' || chr == 'N') {
					geocell += 1;
				}
			}
			geocell = geocell << GeocellLibrary.shiftDistance(resolution);
		} else {
			String source = geostring;
			if (source.length() > MAX_CONVERTIBLE_LENGTH) {
				source = source.substring(0, MAX_CONVERTIBLE_LENGTH);
			}
			resolution = source.length() * GEOMODEL_BIT_WIDTH;
			geocell = Long.parseLong(source, GEOMODEL_BASE_FORMAT) <<
					GeocellLibrary.shiftDistance(resolution);
		}

		geocell += resolution;
		if (resolution % 2 == 0) {
			geocell = bitTwiddler(geocell);
		}
		return geocell;
	}

	/**
	 * Converts bit order to GeoModel bit order.
	 *
	 * GeoModel uses the reverse bit ordering; for even numbered resolutions,
	 * when converting to/from GeoModel strings, the bits must be pairwise
	 * reversed to match the ordering expected by GeoModel.  Otherwise, the
	 * resulting characters are not the same.
	 *
	 * GeoModel has no support for cells of the shape described by odd numbered
	 * resolutions, so bit order does not matter.  It could be argued that it
	 * does not matter when the resolution is even but indivisible by 4, but as
	 * it is a simple size difference, rather than a shape difference, it seems
	 * that converting the bit order is reasonable since the prefixes will then
	 * make sense.
	 *
	 * @param geocell the geocell to convert to geomodel bit order
	 * @return a binary representation in which pairs of bits are flipped
	 */
	public static long bitTwiddler(final long geocell) {
		final int resolution = GeocellLibrary.getResolution(geocell);
		long newGeocell = 0;

		for (int i = 1; i <= resolution; i += 2) {
			newGeocell = newGeocell << 2;
			if (i != resolution && GeocellLibrary.getBit(geocell, i + 1) == 1) {
				newGeocell += 2;
			}
			if (GeocellLibrary.getBit(geocell, i) == 1) {
				newGeocell += 1;
			}
		}
		newGeocell = newGeocell << GeocellLibrary.shiftDistance(resolution);
		return newGeocell + resolution;
	}

	@Override
	public String fromGeocellContainer(final GeocellContainer geocellContainer) {
		return toGeoModelString(geocellContainer.getGeocell());
	}

	@Override
	public long toGeocell(final String convertedGeocell) {
		return fromGeoModelString(convertedGeocell);
	}

	@Override
	public String fromGeocell(final long geocell) {
		return toGeoModelString(geocell);
	}
}
