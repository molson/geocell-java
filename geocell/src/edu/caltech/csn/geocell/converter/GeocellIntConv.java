package edu.caltech.csn.geocell.converter;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.GeocellLibrary;

/**
 * Represent long geocells as ints to save space; limits maximum resolution.
 * <p><ul>
 * <li>Resolution min: GeocellLibrary.MINIMUM_RESOLUTION
 * <li>Resolution max: 27
 * <li>Step size: 1
 * </ul><p>
 * Structure of a geocell int: {up to 27 decision bits}{5 resolution bits}
 *
 * @author Michael Olson <michael.olson@gmail.com>
 */
public final class GeocellIntConv implements Converter<Integer> {

	private static final int INT_MAXIMUM_RESOLUTION = 27;
	private static final int INT_RESOLUTION_MASK = 31;

	public static int fromLongGeocell(final long geocell) {
		final int resolution = Math.min(
				GeocellLibrary.getResolution(geocell),
				INT_MAXIMUM_RESOLUTION);
		final int result = (int)(geocell >>> GeocellLibrary.shiftDistance(resolution))
				<< shiftDistance(resolution);
		return result + resolution;
	}

	public static long toLongGeocell(final int geocell) {
		final int resolution = getResolution(geocell);
		final long result = (long)(geocell >>> shiftDistance(resolution))
				<< GeocellLibrary.shiftDistance(resolution);
		return result + resolution;
	}

	private static int shiftDistance(final int resolution) {
		return Integer.SIZE - resolution;
	}

	private static int getResolution(final int geocell) {
		return (int)(geocell & INT_RESOLUTION_MASK);
	}

	@Override
	public Integer fromGeocellContainer(final GeocellContainer geocellContainer) {
		return Integer.valueOf(fromLongGeocell(geocellContainer.getGeocell()));
	}

	@Override
	public long toGeocell(final Integer convertedGeocell) {
		return toLongGeocell(convertedGeocell.intValue());
	}

	@Override
	public Integer fromGeocell(final long geocell) {
		return fromLongGeocell(geocell);
	}
}
