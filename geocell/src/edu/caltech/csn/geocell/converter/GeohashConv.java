package edu.caltech.csn.geocell.converter;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.GeocellLibrary;

/**
 * Convert to/from Geohash format; limits possible resolutions.
 * <p><ul>
 * <li>Resolution min: 5
 * <li>Resolution max: 55
 * <li>Step size: 5
 * </ul><p>
 * Note: the specified maximum resolution is imposed by the internal
 * representation as a long. The Geohash format can natively support any
 * resolution that is a multiple of 5.
 *
 * @author Michael Olson <michael.olson@gmail.com>
 * @see http://en.wikipedia.org/wiki/Geohash
 */
public class GeohashConv implements Converter<String> {

	private static final int GEOHASH_BIT_WIDTH = 5;
	private static final int MAX_CONVERTIBLE_LENGTH = 11;
	private static final long GEOHASH_MASK = 31L;
	private static final char[] GEOHASH_CHARS = {
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'b', 'c', 'd', 'e',
		'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v',
		'w', 'x', 'y', 'z'
	};

	private static final Map<Character, Integer> GEOHASH_MAP;
	static {
		GEOHASH_MAP = new HashMap<Character, Integer>();
		for (int i = 0; i < GEOHASH_CHARS.length; i++) {
			GEOHASH_MAP.put(GEOHASH_CHARS[i], i);
		}
	}

	/**
	 * Method to construct a geohash string representation of a geocell.
	 * <p>
	 * This method only produces valid geohash objects for resolutions where
	 * <code>resolution % 5 == 0</code>.
	 * <p>
	 * 5 is the number of bits conveyed by a single geohash character.
	 *
	 * @param geocell the geocell to convert to a geohash
	 * @return the geohash string representation of the geocell
	 * @see http://en.wikipedia.org/wiki/Geohash
	 */
	public static String toGeohashString(final long geocell) {
		final int resolution = GeocellLibrary.getResolution(geocell);
		final int geohashLength = (int) (resolution / GEOHASH_BIT_WIDTH);
		final StringBuffer geohash = new StringBuffer();

		long source = geocell >> GeocellLibrary.shiftDistance(resolution);
		for (int i = 0; i < geohashLength; i++) {
			// No support for StringBuffer.reverse() in GWT.
			geohash.insert(0, GEOHASH_CHARS[(int)(source & GEOHASH_MASK)]);
			source = source >>> GEOHASH_BIT_WIDTH;
		}

		return geohash.toString();
	}

	/**
	 * Method to convert a geohash string to a geocell long.
	 * <p>
	 * 11 is the maximum supported length of a geohash string.  A length 12
	 * geohash string would require a possible resolution of 60 to encode.
	 *
	 * @param geohash the geohash string to convert to a geocell
	 * @return the long representation of the input geohash
	 * @see http://en.wikipedia.org/wiki/Geohash
	 */
	public static long fromGeohashString(final String geohash) {
		String source = geohash;
		if (source.length() > MAX_CONVERTIBLE_LENGTH) {
			source = source.substring(0, MAX_CONVERTIBLE_LENGTH);
		}
		source = source.toLowerCase(Locale.US);
		final int resolution = (int) (source.length() * GEOHASH_BIT_WIDTH);

		long geocell = 0;
		for (int i = 0; i < source.length(); i++) {
			geocell = geocell << GEOHASH_BIT_WIDTH;
			geocell += GEOHASH_MAP.get(source.charAt(i));
		}

		geocell = geocell << GeocellLibrary.shiftDistance(resolution);
		return geocell + resolution;
	}

	@Override
	public String fromGeocellContainer(final GeocellContainer geocellContainer) {
		return toGeohashString(geocellContainer.getGeocell());
	}

	@Override
	public long toGeocell(final String convertedGeocell) {
		return fromGeohashString(convertedGeocell);
	}

	@Override
	public String fromGeocell(final long geocell) {
		return toGeohashString(geocell);
	}
}
