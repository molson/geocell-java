package edu.caltech.csn.geocell.converter;

import java.util.HashMap;
import java.util.Map;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.GeocellLibrary;

/**
 * Default string format; supports arbitrary resolutions.
 * <p>
 * Unlike Geohash and GeoModel, which strive for ease of human reading by both
 * avoiding symbols and case sensitivity, this class merely opts for economy of
 * space by using a URL safe Base 64 encoding.  This is necessary because,
 * unlike the aforementioned hashing algorithms, this representation must also
 * store the resolution of the described cell, as the length of the text is
 * insufficient to convey the resolution information.  Recouping as much space
 * as possible is therefore useful.
 * <p><ul>
 * <li>Resolution min: GeocellLibrary.MINIMUM_RESOLUTION
 * <li>Resolution max: GeocellLibrary.MAXIMUM_RESOLUTION
 * <li>Step size: 1
 * </ul><p>
 * Structure of a geostring: {up to 10 decision characters}{1 resolution char}
 * <p>
 * If space is a concern, a comparison of the number of bytes required for
 * different representations is available at the following spreadsheet:
 * @see http://goo.gl/YNnG1
 *
 * @author Michael Olson <michael.olson@gmail.com>
 */
public class GeostringConv implements Converter<String> {

	private static final int GEOSTRING_BIT_WIDTH = 6;
	private static final long GEOSTRING_MASK = 63L;
	private static final char[] GEOSTRING_CHARS = {
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
		'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
		'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		// URL safe alternatives to +, /
		'-', '_'
	};

	private static final Map<Character, Integer> GEOSTRING_MAP;
	static {
		GEOSTRING_MAP = new HashMap<Character, Integer>();
		for (int i = 0; i < GEOSTRING_CHARS.length; i++) {
			GEOSTRING_MAP.put(GEOSTRING_CHARS[i], i);
		}
	}

	/**
	 * Method to construct a geostring representation of a geocell.
	 */
	public static String toGeostring(final long geocell) {
		final int resolution = GeocellLibrary.getResolution(geocell);
		final int geostringLength = (int) Math.ceil((double)resolution / GEOSTRING_BIT_WIDTH);
		final int padding = getPadding(resolution);
		final StringBuffer geostring = new StringBuffer();

		long geocellNoRes = geocell >> GeocellLibrary.shiftDistance(resolution);
		// Zero pad the tail, not the head.
		geocellNoRes = geocellNoRes << padding;
		for (int i = 0; i < geostringLength; i++) {
			// No support for StringBuffer.reverse() in GWT.
			geostring.insert(0, GEOSTRING_CHARS[(int)(geocellNoRes & GEOSTRING_MASK)]);
			geocellNoRes = geocellNoRes >>> GEOSTRING_BIT_WIDTH;
		}
		geostring.insert(0, GEOSTRING_CHARS[resolution]);

		return geostring.toString();
	}

	/**
	 * Method to convert a geostring to a geocell long.
	 */
	public static long fromGeostring(final String geostring) {
		long geocell = 0;
		final int resolution = GEOSTRING_MAP.get(geostring.charAt(0));
		for (int i = 1; i < geostring.length(); i++) {
			geocell = geocell << GEOSTRING_BIT_WIDTH;
			geocell += GEOSTRING_MAP.get(geostring.charAt(i));
		}
		final int padding = getPadding(resolution);
		// Remove zero padding.
		geocell = geocell >> padding;
		geocell = geocell << GeocellLibrary.shiftDistance(resolution);
		return geocell + resolution;
	}

	private static int getPadding(final int resolution) {
		return (GEOSTRING_BIT_WIDTH - (resolution % GEOSTRING_BIT_WIDTH))
				% GEOSTRING_BIT_WIDTH;
	}

	@Override
	public String fromGeocellContainer(final GeocellContainer geocellContainer) {
		return toGeostring(geocellContainer.getGeocell());
	}

	@Override
	public long toGeocell(final String convertedGeocell) {
		return fromGeostring(convertedGeocell);
	}

	@Override
	public String fromGeocell(final long geocell) {
		return toGeostring(geocell);
	}
}
