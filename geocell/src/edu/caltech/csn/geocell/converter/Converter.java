package edu.caltech.csn.geocell.converter;

import edu.caltech.csn.geocell.GeocellContainer;

/**
 * Interface to describe conversion from internal to external representation.
 * <p>
 * This class allows the internal representation of GeocellLibrary to be
 * different from any external representation without a cost in efficiency
 * or flexibility.  Without the conversion class, the internal representation
 * would always be the type returned from functions which return sets.
 * <p>
 * Using the converter class, the internal representation can be changed to the
 * desired external representation while processing is ongoing.  This
 * promotes internal efficiency without limiting the choice of external
 * representation.
 * <p>
 * Prior to the creation of this class, the internal representation was a long,
 * because that was the expected external representation.  However, this
 * resulted in lost efficiency in places where the recalculation of bounds
 * occurred.  This led to a change in the internal representation to a container
 * class which included the geocell's bounds.  Without this converter, a loop
 * over the container class set would be required before any external
 * representation could be achieved.  This class was created to prevent that.
 * <p>
 * Note: The public instance methods were created to make it possible to pass
 * a class as a parameter to a function, but the desired direct use case in
 * most situations is to call the static functions defined by the class.  Thus,
 * for the complicated conversions, the public instance methods are merely
 * aliases to the similar public static methods, enabling both use cases.
 *
 * @author Michael Olson <michael.olson@gmail.com>
 * @param <T> Class of external representation to convert from/to.
 */
public interface Converter<T> {

	/**
	 * Called by getGeocellSet and getQueryCells when compiling cells to return.
	 *
	 * @param geocellContainer
	 * @return
	 */
	T fromGeocellContainer(GeocellContainer geocellContainer);

	/**
	 * Called by Interpolation when merging cells.
	 * <p>
	 * Bounds are not used in merging, and so are not available.  They can
	 * always be reconstructed in this function if needed.
	 *
	 * @param geocell
	 * @return
	 */
	T fromGeocell(long geocell);

	/**
	 * Currently unused by GeocellLibrary.
	 * <p>
	 * Used in testing to verify two-way nature of conversions.
	 *
	 * @param convertedGeocell
	 * @return
	 */
	long toGeocell(T convertedGeocell);

}
