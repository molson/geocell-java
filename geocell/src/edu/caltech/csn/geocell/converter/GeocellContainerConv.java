package edu.caltech.csn.geocell.converter;

import edu.caltech.csn.geocell.GeocellContainer;

/**
 * Conversion to maintain container format, useful if bounds are needed.
 * <p><ul>
 * <li>Resolution min: GeocellLibrary.MINIMUM_RESOLUTION
 * <li>Resolution max: GeocellLibrary.MAXIMUM_RESOLUTION
 * <li>Step size: 1
 * </ul>
 *
 * @author Michael Olson <michael.olson@gmail.com>
 */
public class GeocellContainerConv implements Converter<GeocellContainer> {

	@Override
	public GeocellContainer fromGeocellContainer(final GeocellContainer geocellContainer) {
		// Clone required because containers are mutable.
		return new GeocellContainer(geocellContainer);
	}

	@Override
	public long toGeocell(final GeocellContainer convertedGeocell) {
		return convertedGeocell.getGeocell();
	}

	@Override
	public GeocellContainer fromGeocell(final long geocell) {
		return new GeocellContainer(geocell);
	}
}
