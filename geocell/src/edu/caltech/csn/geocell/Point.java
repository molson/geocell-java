package edu.caltech.csn.geocell;

import java.io.Serializable;

/**
 * Generic Point class for use in Geocell classes.
 *
 * @author Michael Olson <michael.olson@gmail.com>
 */
@SuppressWarnings("serial")
public class Point implements Serializable {
	protected static final double DEG_TO_RADIANS = Math.PI / 180;
	protected static final double RADIANS_TO_DEG = 180 / Math.PI;
//	protected static final double EARTH_RADIUS = 3958.761; // miles
	protected static final double EARTH_RADIUS = 6371.009; // kilometers

	private final double latitude;
	private final double longitude;

	public Point(final double latitude, final double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	/**
	 * Calculate the great circle distance to a given point.
	 * High accuracy formula using a special case of the Vincenty forumla.
	 * <p>
	 * Note: units of returned distance depend on units used for EARTH_RADIUS.
	 *
	 * @param to point to calculate distance to
	 * @return distance to the given point
	 * @see http://en.wikipedia.org/wiki/Great-circle_distance
	 */
	public double distanceTo(final Point to) {
		final double phiS = latitude * DEG_TO_RADIANS;
		final double lambdaS = longitude * DEG_TO_RADIANS;
		final double phiF = to.latitude * DEG_TO_RADIANS;
		final double lambdaF = to.longitude * DEG_TO_RADIANS;
		final double deltaLambda = lambdaF - lambdaS;
		final double cosPhiF = Math.cos(phiF);
		final double cosPhiS = Math.cos(phiS);
		final double sinPhiF = Math.sin(phiF);
		final double sinPhiS = Math.sin(phiS);
		final double cosDeltaLambda = Math.cos(deltaLambda);

		final double x = Math.pow(cosPhiF * Math.sin(deltaLambda), 2);
		final double y = Math.pow(
				cosPhiS * sinPhiF - sinPhiS * cosPhiF * cosDeltaLambda, 2);
		final double b = sinPhiS * sinPhiF + cosPhiS * cosPhiF * cosDeltaLambda;
		return EARTH_RADIUS * Math.atan2(Math.sqrt(x + y), b);
	}

	/**
	 * Calculate the great circle distance to a given point, Haversine.
	 * More compact and slightly faster algorithm based on Haversine formula.
	 * <p>
	 * Latitude/longitude spherical geodesy formulae © 2002-2011 Chris Veness
	 * @see http://www.movable-type.co.uk/scripts/latlong.html
	 *
	 * @param to point to calculate distance to
	 * @return distance to the given point
	 */
	public double distanceToHaversine(final Point to) {
		final double deltaLat = ((to.latitude - latitude) * DEG_TO_RADIANS) / 2;
		final double deltaLon = ((to.longitude - longitude) * DEG_TO_RADIANS) / 2;
		final double a = Math.pow(Math.sin(deltaLat), 2)
					+ Math.pow(Math.sin(deltaLon), 2)
					* Math.cos(latitude * DEG_TO_RADIANS)
					* Math.cos(to.latitude * DEG_TO_RADIANS);
		final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return EARTH_RADIUS * c;
	}

	/**
	 * Calculates the bearing, in compass direction degrees, to the given point.
	 * <p>
	 * Latitude/longitude spherical geodesy formulae © 2002-2011 Chris Veness
	 * @see http://www.movable-type.co.uk/scripts/latlong.html
	 *
	 * @param to the point to calculate the initial bearing toward
	 * @return the bearing, in compass direction degrees, to the given point
	 */
	public double bearingTo(final Point to) {
		final double lat1 = latitude * DEG_TO_RADIANS;
		final double lat2 = to.latitude * DEG_TO_RADIANS;
		final double deltaLon = (to.longitude - longitude) * DEG_TO_RADIANS;
		final double y = Math.sin(deltaLon) * Math.cos(lat2);
		final double x = Math.cos(lat1) * Math.sin(lat2)
				- Math.sin(lat1) * Math.cos(lat2) * Math.cos(deltaLon);
		return (Math.atan2(y, x) * RADIANS_TO_DEG + 360) % 360;
	}

	/**
	 * Calculates the midpoint between two points.
	 * <p>
	 * Latitude/longitude spherical geodesy formulae © 2002-2011 Chris Veness
	 * @see http://www.movable-type.co.uk/scripts/latlong.html
	 *
	 * @param to the point to calculate the midpoint between
	 * @return the midpoint between this point and the given point
	 */
	public Point midpointBetween(final Point to) {
		final double lat1 = latitude * DEG_TO_RADIANS;
		final double lon1 = longitude * DEG_TO_RADIANS;
		final double lat2 = to.latitude * DEG_TO_RADIANS;
		final double deltaLon = (to.longitude - longitude) * DEG_TO_RADIANS;
		final double bx = Math.cos(lat2) * Math.cos(deltaLon);
		final double by = Math.cos(lat2) * Math.sin(deltaLon);
		final double lat = Math.atan2(Math.sin(lat1) + Math.sin(lat2),
				Math.sqrt((Math.cos(lat1) + bx)
						* (Math.cos(lat1) + bx) + Math.pow(by, 2)));
		final double lon = (lon1 + Math.atan2(by, Math.cos(lat1) + bx)
				+ 3 * Math.PI) % (2 * Math.PI) - Math.PI;

		return new Point(lat * RADIANS_TO_DEG, lon * RADIANS_TO_DEG);
	}

	/**
	 * Returns the destination point after traveling the given distance (in km)
	 * on the provided initial bearing (may vary before destination).
	 * <p>
	 * Latitude/longitude spherical geodesy formulae © 2002-2011 Chris Veness
	 * @see http://www.movable-type.co.uk/scripts/latlong.html
	 *
	 * @param bearing initial bearing in degrees
	 * @param distance distance in km
	 * @return destination point
	 */
	public Point destinationPoint(final double bearing, final double distance) {
		final double dist = distance / EARTH_RADIUS;
		final double brng = bearing * DEG_TO_RADIANS;
		final double lat1 = latitude * DEG_TO_RADIANS;
		final double lon1 = longitude * DEG_TO_RADIANS;
		final double lat2 = Math.asin(
				Math.sin(lat1) * Math.cos(dist)
				+ Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));
		final double lon2 = (lon1 + Math.atan2(
				Math.sin(brng) * Math.sin(dist) * Math.cos(lat1),
				Math.cos(dist) - Math.sin(lat1) * Math.sin(lat2))
					+ 3 * Math.PI) % (2 * Math.PI) - Math.PI;

		return new Point(lat2 * RADIANS_TO_DEG, lon2 * RADIANS_TO_DEG);
	}

	public String toString() {
		return "(" + latitude + ", " + longitude + ")";
	}

	public boolean equals(final Object obj) {
		if (!(obj instanceof Point)) {
			return false;
		}
		final Point b = (Point)obj;
		return (latitude == b.latitude && longitude == b.longitude);
	}

	public int hashCode() {
		final int prime = 2393;
		int hash = 1292;
		hash = hash * prime + Double.valueOf(latitude).hashCode();
		hash = hash * prime + Double.valueOf(longitude).hashCode();
		return hash;
	}

	public double getLatitude() { return latitude; }
	public double getLongitude() { return longitude; }
}
