package edu.caltech.csn.geocell;

import java.io.Serializable;

/**
 * Generic bounds class for use in Geocell classes.
 *
 * @author Michael Olson <michael.olson@gmail.com>
 */
@SuppressWarnings("serial")
public class Bounds implements Serializable, Cloneable {
	private static final double AREA_COEFFICIENT =
			Math.PI / 180 * Math.pow(Point.EARTH_RADIUS, 2);

	// Used as state variables in Geocell library, so they are protected only.
	protected double swLat;
	protected double swLon;
	protected double neLat;
	protected double neLon;

	public Bounds(final Point southWest, final Point northEast) {
		this.swLat = southWest.getLatitude();
		this.swLon = southWest.getLongitude();
		this.neLat = northEast.getLatitude();
		this.neLon = northEast.getLongitude();
	}

	public Bounds(final double swLat, final double swLon, final double neLat, final double neLon) {
		this.swLat = swLat;
		this.swLon = swLon;
		this.neLat = neLat;
		this.neLon = neLon;
	}

	public Bounds(final Bounds source) {
		this.swLat = source.swLat;
		this.swLon = source.swLon;
		this.neLat = source.neLat;
		this.neLon = source.neLon;
	}

	public static Bounds fromPointAndRadius(final Point pt, final double radius) {
		final Point deg0 = pt.destinationPoint(0d, radius);
		final Point deg90 = pt.destinationPoint(90d, radius);
		final Point deg180 = pt.destinationPoint(180d, radius);
		final Point deg270 = pt.destinationPoint(270d, radius);
		return new Bounds(deg180.getLatitude(), deg270.getLongitude(),
				deg0.getLatitude(), deg90.getLongitude());
	}

	/**
	 * Calculates reasonable approximation of the midpoint of the bounds.
	 * <p>
	 * Used in particular for finding the center of a 2d map boundary.
	 * A different approximation:
	 *
	 * <pre><code> Point leftCenter = getSouthWest().midpointTo(getNorthEast());
	 * Point rightCenter = getNorthWest().midpointTo(getSouthEast());
	 * Point center = leftCenter.midpointTo(rightCenter);</code></pre>
	 *
	 * @return midpoint of the bounds
	 */
	public Point getCenter() {
		// Convert latitude/longitude to Cartesian coordinates for each point
		// and compute average x and y coordinates.
		final double lat1 = swLat * Point.DEG_TO_RADIANS;
		final double lon1 = swLon * Point.DEG_TO_RADIANS;
		final double lat2 = neLat * Point.DEG_TO_RADIANS;
		final double lon2 = neLon * Point.DEG_TO_RADIANS;
		final double sumLatCos = Math.cos(lat1) + Math.cos(lat2);
		// CHECKSTYLE IGNORE MagicNumber FOR NEXT 2 LINES.
		final double avgX = sumLatCos * (Math.cos(lon1) + Math.cos(lon2)) / 4;
		final double avgY = sumLatCos * (Math.sin(lon1) + Math.sin(lon2)) / 4;

		// Convert average x and y coordinates to longitude.
		double lon = Math.atan2(avgY, avgX) * Point.RADIANS_TO_DEG;

		// Latitude is easy for the purposes of this calculation since the
		// distance between degrees of latitude is nearly constant.
		final double lat = (neLat + swLat) / 2;

		final Point center = new Point(lat, lon);

		// If the bearing is greater than or equal to 270, then we got the
		// center from the wrong direction; flip the longitude.
		// This is only possible for bounds that contain most of the earth
		// where the great circle distance between the southwest and northeast
		// points would not go anywhere near the center of a 2d projection.
		if (getSouthWest().bearingTo(center) >= 270) {
			lon += 180;
			if (lon > 180) {
				lon -= 360;
			}
			return new Point(lat, lon);
		} else {
			return center;
		}
	}

	public boolean contains(final Point pt) {
		final double lat = pt.getLatitude();
		final double lon = pt.getLongitude();

		if (lat >= swLat && lat <= neLat) {
			// Bounds contain the 180th meridian.
			if (swLon > neLon) {
				return lon < neLon || lon > swLon;
			} else {
				return lon < neLon && lon > swLon;
			}
		} else {
			return false;
		}
	}

	/**
	 * Return area contained by bounds.
	 *
	 * @return area contained by the bounds
	 */
	protected double getArea() {
		final double lat1 = swLat * Point.DEG_TO_RADIANS;
		final double lat2 = neLat * Point.DEG_TO_RADIANS;

		return AREA_COEFFICIENT
				* Math.abs(Math.sin(lat1) - Math.sin(lat2))
				* Math.abs(swLon - neLon);
	}

	public double getSwLat() { return swLat; }
	public double getSwLon() { return swLon; }
	public double getNeLat() { return neLat; }
	public double getNeLon() { return neLon; }
	public Point getSouthWest() { return new Point(swLat, swLon); }
	public Point getNorthEast() { return new Point(neLat, neLon); }
	public Point getSouthEast() { return new Point(swLat, neLon); }
	public Point getNorthWest() { return new Point(neLat, swLon); }

	public String toString() {
		return "( " + getSouthWest() + ", " + getNorthEast() + " )";
	}

	public boolean equals(final Object otherObj) {
		if (!(otherObj instanceof Bounds)) {
			return false;
		}
		final Bounds other = (Bounds)otherObj;
		// This is intentionally an exact comparison.
		return Double.compare(swLat, other.swLat) == 0 &&
			Double.compare(swLon, other.swLon) == 0 &&
			Double.compare(neLat, other.neLat) == 0 &&
			Double.compare(neLon, other.neLon) == 0;
	}

	public int hashCode() {
		final int prime = 7477;
		int hash = 6291;
		hash = hash * prime + Double.valueOf(swLat).hashCode();
		hash = hash * prime + Double.valueOf(swLon).hashCode();
		hash = hash * prime + Double.valueOf(neLat).hashCode();
		hash = hash * prime + Double.valueOf(neLon).hashCode();
		return hash;
	}
}
