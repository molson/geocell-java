package edu.caltech.csn.geocell;

import java.io.Serializable;

import edu.caltech.csn.geocell.converter.BinaryStringConv;

/**
 * Container object with the bounds and initialization point for a geocell.
 *
 * @author Michael Olson <michael.olson@gmail.com>
 */
@SuppressWarnings("serial")
public class GeocellContainer implements Serializable, Comparable<GeocellContainer> {

	// Used as state variables in Geocell library, so they are protected only.
	protected long geocell = 0;
	protected double latitude;
	protected double longitude;
	protected Bounds bounds = null;
	protected boolean point = false;

	public GeocellContainer(final String geostring) {
		geocell = GeocellLibrary.fromString(geostring);
		bounds = GeocellLibrary.recalculateBounds(geocell);
	}

	public GeocellContainer(final long geocell) {
		this.geocell = geocell;
		bounds = GeocellLibrary.recalculateBounds(geocell);
	}

	public GeocellContainer(final double latitude, final double longitude) {
		this(latitude, longitude, GeocellLibrary.MAX_RESOLUTION);
	}

	public GeocellContainer(final double latitude, final double longitude, final int res) {
		bounds = new Bounds(GeocellLibrary.WORLD_BOUNDS);
		this.latitude = latitude;
		this.longitude = longitude;
		this.point = true;
		GeocellLibrary.increaseResolution(this, res);
	}

	public GeocellContainer(final Point pt) {
		this(pt, GeocellLibrary.MAX_RESOLUTION);
	}

	public GeocellContainer(final Point pt, final int res) {
		this(pt.getLatitude(), pt.getLongitude(), res);
	}

	public GeocellContainer(final GeocellContainer source) {
		GeocellContainer.copy(source, this);
	}

	/**
	 * Protected constructor used by this class and GeocellLibrary.
	 */
	protected GeocellContainer() {
		// Members set by caller.
	}

	/**
	 * Returns a two dimensional array of doubles representing the bounding box.
	 *
	 * @return 2x2 array of doubles: 0,0 = sw lat, 0,1 = sw lon,
	 *         1,0 = ne lat, 1,1 = ne lon.
	 */
	public Bounds getBounds() {
		return bounds;
	}

	/**
	 * Getter function for Geocell's numerical value.
	 *
	 * @return long representation of Geocell
	 */
	public long getGeocell() {
		return geocell;
	}

	/**
	 * Getter function for Geocell's resolution.
	 *
	 * @return int representing current resolution of Geocell
	 */
	public int getResolution() {
		return GeocellLibrary.getResolution(geocell);
	}

	/**
	 * Modify the resolution of the geocell.
	 * <p>
	 * This function allows a Geocell to have its resolution modified.
	 * Geocells constructed from a point can have their resolution increased,
	 * while Geocells constructed from a geocell long or by shifting an
	 * existing Geocell can only have their resolution decreased
	 * (irreversibly).  This function is most useful in work saving,
	 * particularly in progressively increasing the resolution of a Geocell.
	 *
	 * @param newRes
	 */
	public void changeResolution(final int newRes) {
		final int resolution = getResolution();
		if (newRes > resolution) {
			if (point) {
				GeocellLibrary.increaseResolution(this, newRes);
			}
			// TODO: Should the else statement thrown an exception?
		} else if (newRes < resolution) {
			GeocellLibrary.decreaseResolution(this, newRes);
		}
	}

	/**
	 * Determines the equality of two cells by their long representations.
	 * <p>
	 * Long representations include the resolution.
	 * <p>
	 * The value of point, latitude, and longitude are purposefully not
	 * checked.  These are stored for convenience of increasing resolution.
	 * This function will return true even if the cells were initialized with
	 * different point sources.
	 * <p>
	 * The bounds do not need to be checked, as it would be a severe bug with
	 * the library if two identical geocells had different bounds.
	 *
	 * @param obj the cell to determine equality with
	 * @return true if the provided cell has an equal resolution and geocell
	 */
	public boolean equals(final Object obj) {
		if (!(obj instanceof GeocellContainer)) {
			return false;
		}
		return this.geocell == ((GeocellContainer)obj).geocell;
	}

	public int hashCode() {
		return Long.valueOf(geocell).hashCode();
	}

	/**
	 * Changes this object to be the geocell immediately north at this res.
	 * <p>
	 * This function modifies the existing object.  If you need a copy of the
	 * current geocell and the shifted geocell, clone this cell first.
	 * <p>
	 * Acting as an extension of the geocell library, this function accesses
	 * protected members of the bounds class with impunity.
	 */
	public void moveNorth() {
		final double latSpan = bounds.neLat - bounds.swLat;
		geocell = GeocellLibrary.getNorth(geocell);
		bounds.swLat = bounds.neLat;
		bounds.neLat += latSpan;
		if (bounds.neLat > 90) {
			bounds.neLat -= 180;
		}
	}

	/**
	 * Changes this object to be the geocell immediately south at this res.
	 * <p>
	 * This function modifies the existing object.  If you need a copy of the
	 * current geocell and the shifted geocell, clone this cell first.
	 * <p>
	 * Acting as an extension of the geocell library, this function accesses
	 * protected members of the bounds class with impunity.
	 */
	public void moveSouth() {
		final double latSpan = bounds.neLat - bounds.swLat;
		geocell = GeocellLibrary.getSouth(geocell);
		bounds.neLat = bounds.swLat;
		bounds.swLat -= latSpan;
		if (bounds.swLat < -90) {
			bounds.swLat += 180;
		}
	}

	/**
	 * Changes this object to be the geocell immediately east at this res.
	 * <p>
	 * This function modifies the existing object.  If you need a copy of the
	 * current geocell and the shifted geocell, clone this cell first.
	 * <p>
	 * Acting as an extension of the geocell library, this function accesses
	 * protected members of the bounds class with impunity.
	 */
	public void moveEast() {
		final double lonSpan = bounds.neLon - bounds.swLon;
		geocell = GeocellLibrary.getEast(geocell);
		bounds.swLon = bounds.neLon;
		bounds.neLon += lonSpan;
		if (bounds.neLon >= 180) {
			bounds.neLon -= 360;
		} else if (bounds.neLon < -180) {
			bounds.neLon += 360;
		}
	}

	/**
	 * Changes this object to be the geocell immediately west at this res.
	 * <p>
	 * This function modifies the existing object.  If you need a copy of the
	 * current geocell and the shifted geocell, clone this cell first.
	 * <p>
	 * Acting as an extension of the geocell library, this function accesses
	 * protected members of the bounds class with impunity.
	 */
	public void moveWest() {
		final double lonSpan = bounds.neLon - bounds.swLon;
		geocell = GeocellLibrary.getWest(geocell);
		bounds.neLon = bounds.swLon;
		bounds.swLon -= lonSpan;
		if (bounds.swLon > 180) {
			bounds.swLon -= 360;
		} else if (bounds.swLon < -180) {
			bounds.swLon += 360;
		}
	}

	/**
	 * Used by GwtGeocell constructor and clone for consistency.
	 */
	protected static void copy(final GeocellContainer source, final GeocellContainer dest) {
		dest.geocell = source.geocell;
		dest.latitude = source.latitude;
		dest.longitude = source.longitude;
		dest.bounds = new Bounds(source.bounds);
		dest.point = source.point;
	}

	/**
	 * Alias for the toString function of the Geocell Library.
	 * <p>
	 * Bounds can be reconstructed, so the only information lost in this string
	 * conversion is the creation point.
	 *
	 * @return string string representation of the geocell of this container
	 */
	public String toString() {
		return GeocellLibrary.toString(geocell);
	}

	/**
	 * Provides information about the other member objects of the container.
	 * <p>
	 * Mostly for finer detail in logging and debugging.
	 *
	 * @return string geocell with its bounds and creation point
	 */
	protected String toVerboseString(final boolean includePoint, final boolean includeBin) {
		final StringBuffer result = new StringBuffer();
		result.append(GeocellLibrary.toString(geocell));
		result.append(" : ").append(bounds);
		if (includePoint && point) {
			result.append(" : ").append(new Point(latitude, longitude));
		}
		if (includeBin) {
			result.append(" : ").append(BinaryStringConv.toBinaryString(geocell));
		}
		return result.toString();
	}

	// Necessary for adding containers to TreeSet objects to work.
	@Override
	public int compareTo(final GeocellContainer arg0) {
		return Long.valueOf(geocell).compareTo(arg0.geocell);
	}
}
