package edu.caltech.csn.geocell.converter;

import org.junit.Test;

import static org.junit.Assert.*;

import edu.caltech.csn.geocell.GeocellContainer;
import edu.caltech.csn.geocell.Point;
import edu.caltech.csn.test.TestLocations;


public class GeocellIntConvTest {

	@Test
	public void testToFromLongGeocell() {
		final Converter<Integer> converter = new GeocellIntConv();
		for (Point pt : TestLocations.getLocations().values()) {
			final GeocellContainer gc = new GeocellContainer(pt, 1);

			// For the entire range of GeocellInt resolutions.
			for (int i = 1; i < 27; i++) {
				gc.changeResolution(i);
				final int t = converter.fromGeocellContainer(gc);
				final long geocell = converter.toGeocell(t);
				assertEquals(gc.getGeocell(), geocell);
			}
		}
	}

}
