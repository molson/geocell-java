package edu.caltech.csn.geocell;

import java.util.logging.Logger;

import org.junit.Test;

import edu.caltech.csn.test.TestLocations;

public class PointTest {

	private static final Logger LOG = Logger.getLogger(BoundsTest.class.getName());

	// Not currently a real test, just outputs some figures.
	@Test
	public void testDistanceBearing() {
		final Point newYork = new Point(TestLocations.dmsToDecDeg("40°43′N"),
				TestLocations.dmsToDecDeg("74°00′W"));
		final Point london = new Point(TestLocations.dmsToDecDeg("51°30′N"),
				TestLocations.dmsToDecDeg("0°08′W"));
		final Point buenosAires = new Point(TestLocations.dmsToDecDeg("34°40′S"),
				TestLocations.dmsToDecDeg("58°24′W"));
		final Point sydney = new Point(TestLocations.dmsToDecDeg("33°52′S"),
				TestLocations.dmsToDecDeg("151°13′E"));

		LOG.fine("New York to:");
		LOG.fine("London: "
				+ newYork.distanceTo(london) + " | "
				+ newYork.distanceToHaversine(london) + " | "
				+ newYork.bearingTo(london));
		LOG.fine("Buenos Aires: "
				+ newYork.distanceTo(buenosAires) + " | "
				+ newYork.distanceToHaversine(buenosAires) + " | "
				+ newYork.bearingTo(buenosAires));
		LOG.fine("Sydney: "
				+ newYork.distanceTo(sydney) + " | "
				+ newYork.distanceToHaversine(sydney) + " | "
				+ newYork.bearingTo(sydney));
		LOG.fine("Buenos Aires to:");
		LOG.fine("London: "
				+ buenosAires.distanceTo(london) + " | "
				+ buenosAires.distanceToHaversine(london) + " | "
				+ buenosAires.bearingTo(london));
		Point midpoint = buenosAires.midpointBetween(london);
		LOG.fine("London Midpoint " + midpoint + ": "
				+ buenosAires.distanceTo(midpoint) + " | "
				+ buenosAires.distanceToHaversine(midpoint) + " | "
				+ buenosAires.bearingTo(midpoint));
		LOG.fine("New York: "
				+ buenosAires.distanceTo(newYork) + " | "
				+ buenosAires.distanceToHaversine(newYork) + " | "
				+ buenosAires.bearingTo(newYork));
		midpoint = buenosAires.midpointBetween(newYork);
		LOG.fine("New York Midpoint " + midpoint + ": "
				+ buenosAires.distanceTo(midpoint) + " | "
				+ buenosAires.distanceToHaversine(midpoint) + " | "
				+ buenosAires.bearingTo(midpoint));

		long startTime = System.currentTimeMillis();
		for (int i = 0; i < 1000000; i++) {
			newYork.distanceTo(sydney);
		}
		final long alg1dur = System.currentTimeMillis() - startTime;

		startTime = System.currentTimeMillis();
		for (int i = 0; i < 1000000; i++) {
			newYork.distanceToHaversine(sydney);
		}
		final long alg2dur = System.currentTimeMillis() - startTime;

		LOG.fine("Durations: " + alg1dur + " | " + alg2dur);
	}

}
