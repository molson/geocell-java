package edu.caltech.csn.geocell;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import static org.junit.Assert.*;

import edu.caltech.csn.geocell.converter.BinaryStringConv;
import edu.caltech.csn.geocell.converter.Converter;
import edu.caltech.csn.geocell.converter.GeoModelConv;
import edu.caltech.csn.geocell.converter.GeohashConv;
import edu.caltech.csn.geocell.converter.GeostringConv;
import edu.caltech.csn.test.TestLocations;

public class GeocellLibraryTest {

	// For single point tests.
	private static final Point SAN_FRAN = new Point(37.78061965350541d, -122.42425918579102d);

	/**
	 * Test predating unit tests that was implemented to mimic navigating a
	 * known map of San Francisco.
	 */
	@Test
	public void testWanderWithGeomodel() {
		final Converter<String> geoModelConv = new GeoModelConv();
		// Get geocell for San Francisco at Geomodel resolution 7.
		long geocell = GeocellLibrary.getGeocell(SAN_FRAN, 28);
		final long start = geocell;

		// Wander . . .
		final String sfStart = "8e62df8";
		assertEquals(sfStart, geoModelConv.fromGeocell(geocell));
		assertEquals(start, geocell);

		// East and west to start.
		geocell = GeocellLibrary.getEast(geocell);
		assertEquals("8e62df9", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getEast(geocell);
		assertEquals("8e62dfc", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getEast(geocell);
		assertEquals("8e62dfd", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getEast(geocell);
		assertEquals("8e638a8", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getEast(geocell);
		assertEquals("8e638a9", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getWest(geocell);
		assertEquals("8e638a8", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getWest(geocell);
		assertEquals("8e62dfd", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getWest(geocell);
		assertEquals("8e62dfc", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getWest(geocell);
		assertEquals("8e62df9", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getWest(geocell);
		assertEquals(sfStart, geoModelConv.fromGeocell(geocell));
		assertEquals(start, geocell);

		// North and south to start.
		geocell = GeocellLibrary.getNorth(geocell);
		assertEquals("8e62dfa", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getNorth(geocell);
		assertEquals("8e62f50", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getNorth(geocell);
		assertEquals("8e62f52", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getNorth(geocell);
		assertEquals("8e62f58", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getNorth(geocell);
		assertEquals("8e62f5a", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getSouth(geocell);
		assertEquals("8e62f58", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getSouth(geocell);
		assertEquals("8e62f52", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getSouth(geocell);
		assertEquals("8e62f50", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getSouth(geocell);
		assertEquals("8e62dfa", geoModelConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getSouth(geocell);
		assertEquals(sfStart, geoModelConv.fromGeocell(geocell));
		assertEquals(start, geocell);

		// Wander back to start.
		geocell = GeocellLibrary.getWest(geocell);
		geocell = GeocellLibrary.getNorth(geocell);
		geocell = GeocellLibrary.getEast(geocell);
		geocell = GeocellLibrary.getEast(geocell);
		geocell = GeocellLibrary.getSouth(geocell);
		geocell = GeocellLibrary.getSouth(geocell);
		geocell = GeocellLibrary.getWest(geocell);
		geocell = GeocellLibrary.getWest(geocell);
		geocell = GeocellLibrary.getNorth(geocell);
		geocell = GeocellLibrary.getEast(geocell);
		assertEquals(sfStart, geoModelConv.fromGeocell(geocell));
		assertEquals(start, geocell);
	}

	/**
	 * Test predating unit tests that was implemented to mimic navigating a
	 * known map of San Francisco.
	 */
	@Test
	public void testWanderWithGeohash() {
		final Converter<String> geohashConv = new GeohashConv();
		long geocell = GeocellLibrary.getGeocell(57.64911, 10.40744, 50);
		assertEquals(geohashConv.toGeocell("u4pruydqqv"), geocell);

		// Trot around San Francisco using geohashes.
		geocell = geohashConv.toGeocell("9q8yyj");
		assertEquals("9q8yyj", geohashConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getEast(geocell);
		assertEquals("9q8yym", geohashConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getEast(geocell);
		assertEquals("9q8yyt", geohashConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getSouth(geocell);
		assertEquals("9q8yys", geohashConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getSouth(geocell);
		assertEquals("9q8yye", geohashConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getWest(geocell);
		assertEquals("9q8yy7", geohashConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getNorth(geocell);
		assertEquals("9q8yyk", geohashConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getSouth(geocell);
		assertEquals("9q8yy7", geohashConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getWest(geocell);
		assertEquals("9q8yy5", geohashConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getNorth(geocell);
		assertEquals("9q8yyh", geohashConv.fromGeocell(geocell));
		geocell = GeocellLibrary.getNorth(geocell);
		assertEquals("9q8yyj", geohashConv.fromGeocell(geocell));
	}

	@Test
	public void testIncreaseResolution() {
		for (Point pt : TestLocations.getLocations().values()) {
			final GeocellContainer geocell = new GeocellContainer(
					pt, GeocellLibrary.MIN_RESOLUTION);
			for (int i = GeocellLibrary.MIN_RESOLUTION;
					i <= GeocellLibrary.MAX_RESOLUTION;
					i++) {
				GeocellLibrary.increaseResolution(geocell, i);
				assertEquals(GeocellLibrary.getGeocell(pt, i),
						geocell.getGeocell());
			}
		}
	}

	@Test
	public void testDecreaseResolutionLongInt() {
		for (Point pt : TestLocations.getLocations().values()) {
			long geocell = GeocellLibrary.getGeocell(
					pt, GeocellLibrary.MAX_RESOLUTION);
			for (int i = GeocellLibrary.MAX_RESOLUTION;
					i >= GeocellLibrary.MIN_RESOLUTION;
					i--) {
				geocell = GeocellLibrary.decreaseResolution(geocell, i);
				assertEquals(GeocellLibrary.getGeocell(pt, i), geocell);
			}
		}
	}

	@Test
	public void testDecreaseResolutionGeocellContainerInt() {
		for (Point pt : TestLocations.getLocations().values()) {
			final GeocellContainer geocell = new GeocellContainer(
					pt, GeocellLibrary.MAX_RESOLUTION);
			for (int i = GeocellLibrary.MAX_RESOLUTION;
					i >= GeocellLibrary.MIN_RESOLUTION;
					i--) {
				GeocellLibrary.decreaseResolution(geocell, i);
				assertEquals(GeocellLibrary.getGeocell(pt, i),
						geocell.getGeocell());
			}
		}
	}

	@Test
	public void testGetResolution() {
		final long t = Long.MAX_VALUE;
		assertEquals(63, GeocellLibrary.getResolution(t));
	}

	@Test
	public void testGetBit() {
		long t = 1L << GeocellLibrary.shiftDistance(1);
		assertEquals(1, GeocellLibrary.getBit(t, 1));
		t = 1L << GeocellLibrary.shiftDistance(3);
		assertEquals(1, GeocellLibrary.getBit(t, 3));
		t = 1L << GeocellLibrary.shiftDistance(5);
		assertEquals(1, GeocellLibrary.getBit(t, 5));
	}

	@Test
	public void testGetEast() {
		int width = 1;
		final GeocellContainer geocell = new GeocellContainer(SAN_FRAN, 1);
		for (int i = 1; i < 23; i += 2) {
			// Width of world in geocells at resolution i.
			width = width << 1;
			geocell.changeResolution(i);
			long t = geocell.getGeocell();
			boolean before = GeocellLibrary.getBit(t, i) == 1;
			for (int j = 0; j < width; j++) {
				t = GeocellLibrary.getEast(t);
				final boolean after = GeocellLibrary.getBit(t, i) == 1;
				// Longitude bit flips with every move.
				assertEquals(before, !after);
				before = after;
			}
			// Always return to start after wrapping the world.
			assertEquals(geocell.getGeocell(), t);
		}
	}

	@Test
	public void testGetWest() {
		int width = 1;
		final GeocellContainer geocell = new GeocellContainer(SAN_FRAN, 1);
		for (int i = 1; i < 23; i += 2) {
			// Width of world in geocells at resolution i.
			width = width << 1;
			geocell.changeResolution(i);
			long t = geocell.getGeocell();
			boolean before = GeocellLibrary.getBit(t, i) == 1;
			for (int j = 0; j < width; j++) {
				t = GeocellLibrary.getWest(t);
				final boolean after = GeocellLibrary.getBit(t, i) == 1;
				// Longitude bit flips with every move.
				assertEquals(before, !after);
				before = after;
			}
			// Always return to start after wrapping the world.
			assertEquals(geocell.getGeocell(), t);
		}
	}

	@Test
	public void testGetNorth() {
		int height = 1;
		final GeocellContainer geocell = new GeocellContainer(SAN_FRAN, 2);
		for (int i = 2; i < 24; i += 2) {
			// Height of world in geocells at resolution i.
			height = height << 1;
			geocell.changeResolution(i);
			long t = geocell.getGeocell();
			boolean before = GeocellLibrary.getBit(t, i) == 1;
			for (int j = 0; j < height; j++) {
				t = GeocellLibrary.getNorth(t);
				final boolean after = GeocellLibrary.getBit(t, i) == 1;
				// Latitude bit flips with every move.
				assertEquals(before, !after);
				before = after;
			}
			// FIXME: getNorth/getSouth should not wrap.
			assertEquals(geocell.getGeocell(), t);
		}
	}

	@Test
	public void testGetSouth() {
		int height = 1;
		final GeocellContainer geocell = new GeocellContainer(SAN_FRAN, 2);
		for (int i = 2; i < 24; i += 2) {
			// Height of world in geocells at resolution i.
			height = height << 1;
			geocell.changeResolution(i);
			long t = geocell.getGeocell();
			boolean before = GeocellLibrary.getBit(t, i) == 1;
			for (int j = 0; j < height; j++) {
				t = GeocellLibrary.getSouth(t);
				final boolean after = GeocellLibrary.getBit(t, i) == 1;
				// Latitude bit flips with every move.
				assertEquals(before, !after);
				before = after;
			}
			// FIXME: getNorth/getSouth should not wrap.
			assertEquals(geocell.getGeocell(), t);
		}
	}

	@Test
	public void testGetGeocellSetDoubleDouble() {
		final Set<Long> geocells = GeocellLibrary.getGeocellSet(
				SAN_FRAN.getLatitude(), SAN_FRAN.getLongitude());
		final GeocellContainer sf = new GeocellContainer(SAN_FRAN, 1);
		for (int i = GeocellLibrary.MIN_USEFUL_RESOLUTION;
				i <= GeocellLibrary.MAX_USEFUL_RESOLUTION;
				i += GeocellLibrary.RESOLUTION_STEP_SIZE) {
			sf.changeResolution(i);
			assertTrue(geocells.contains(sf.getGeocell()));
		}
	}

	@Test
	public void testGetGeocellSetDoubleDoubleConverterOfT() {
		final BinaryStringConv conv = new BinaryStringConv();
		final Set<String> geocells = GeocellLibrary.getGeocellSet(
				SAN_FRAN.getLatitude(), SAN_FRAN.getLongitude(), conv);
		final GeocellContainer sf = new GeocellContainer(SAN_FRAN, 1);
		for (int i = GeocellLibrary.MIN_USEFUL_RESOLUTION;
				i <= GeocellLibrary.MAX_USEFUL_RESOLUTION;
				i += GeocellLibrary.RESOLUTION_STEP_SIZE) {
			sf.changeResolution(i);
			assertTrue(geocells.contains(conv.fromGeocellContainer(sf)));
		}
	}

	@Test
	public void testGetQueryCellsDoubleDoubleDoubleDouble() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetQueryCellsDoubleDoubleDoubleDoubleConverterOfT() {
		fail("Not yet implemented");
	}

	@Test
	public void testInterpolation() {
		fail("Not yet implemented");
	}

	@Test
	public void testFixedGrid() {
		final Set<String> expected = new HashSet<String>();
		expected.add("RTYu");
		expected.add("RTYs");
		expected.add("RTPU");
		expected.add("RTN8");
		expected.add("RTYq");
		expected.add("RTYo");
		expected.add("RTaG");
		expected.add("RTN-");
		expected.add("RTaE");
		expected.add("RTPW");
		expected.add("RTaC");
		expected.add("RTaA");
		final Converter<String> converter = new GeostringConv();
		final Point pasadena = new Point(34.156098d, -118.131808d);
		final Bounds bnds = Bounds.fromPointAndRadius(pasadena, 100);
		final Set<String> cells = GeocellLibrary.fixedGrid(bnds,  17, converter);
		assertEquals(expected, cells);
	}

	@Test
	public void testRecalculateBounds() {
		fail("Not yet implemented");
	}

	@Test
	public void testContains() {
		fail("Not yet implemented");
	}

	@Test
	public void testAncestor() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsAncestorOf() {
		fail("Not yet implemented");
	}

	@Test
	public void testChildren() {
		fail("Not yet implemented");
	}

	@Test
	public void testChildrenAtResolution() {
		fail("Not yet implemented");
	}

	@Test
	public void testToStringLong() {
		fail("Not yet implemented");
	}

	@Test
	public void testFromString() {
		fail("Not yet implemented");
	}
}
