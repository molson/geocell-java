package edu.caltech.csn.gwt.client;

import org.junit.Before;
import org.junit.Test;

import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.maps.client.Maps;
import com.google.gwt.maps.client.geom.LatLng;

/**
 * Execute tests on GwtGeocell objects.
 *
 * Default classpath is insufficient for GWT unit tests.
 * @see http://raibledesigns.com/rd/entry/testing_gwt_applications
 *
 * @author Michael Olson <michael.olson@gmail.com>
 */
public class GwtGeocellTest extends GWTTestCase {

	@Before
	public void setup() {
		// CHECKSTYLE IGNORE LineLength FOR NEXT 1 LINE.
		final String apikey = "ABQIAAAAixbG_hXVnpTGOxnOjWnewBRYE4tH-IKD7w7_MzELdjmP9mj85hTWBoZy2axYp7n9WVrvRoM84DwA-w";
		Maps.loadMapsApi(apikey, "2", false, new Runnable() {
			public void run() {
			}
		});
	}

	@Override
	public String getModuleName() {
		return "edu.caltech.csn.gwt.GeocellMap";
	}

	@Test
	public void testCopy() {
		final LatLng sanFran = LatLng.newInstance(37.78061965350541d, -122.42425918579102d);
		final GwtGeocell cell = new GwtGeocell(sanFran, 56);
		final GwtGeocell ref = new GwtGeocell(cell);
		assertTrue(cell.debugEquals(ref));
	}

	@Test
	public void testChangeResolution() {
		final LatLng sanFran = LatLng.newInstance(37.78061965350541d, -122.42425918579102d);
		final GwtGeocell cell = new GwtGeocell(sanFran, 56);

		for (int i = 4; i <= 58; i++) {
			cell.changeResolution(i);
			assertTrue(cell.debugEquals(new GwtGeocell(sanFran, i)));
		}
	}

}
