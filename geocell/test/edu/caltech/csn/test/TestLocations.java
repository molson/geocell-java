package edu.caltech.csn.test;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import edu.caltech.csn.geocell.Point;


public final class TestLocations {

	private TestLocations() {
	}

	// List of locations to use for testing so that all tests are not done on
	// a single point.
	private static final Map<String,Point> LOCATIONS;

	static {
		LOCATIONS = new HashMap<String,Point>();
		// Far North
		LOCATIONS.put("Alert, Canada", getPoint("82°28′N	62°30′"));
		LOCATIONS.put("Longyearbyen, Norway", getPoint("78°13′N	15°34'E"));
		// Far South (not compared to the northern point)
		LOCATIONS.put("Puerto Toro, Chile", getPoint("55º01′S	67º06′W"));
		LOCATIONS.put("Invercargill, New Zealand", getPoint("46°25′S	168°19′E"));
		// Equator
		LOCATIONS.put("Pontianak, Indonesia", getPoint("0°00′N/S	109°20′E"));
		LOCATIONS.put("Macapá, Brazil", getPoint("0°02'N	51°03'W"));
		LOCATIONS.put("Quito, Ecuador", getPoint("0°15′S	78°35′W"));
		// Prime Meridian
		LOCATIONS.put("Greenwich, England", getPoint("51º28′N	0º0′E"));
		LOCATIONS.put("London, England", getPoint("51º30′N	0º07′W"));
		LOCATIONS.put("Lomé, Togo", getPoint("6°08′N	1°12′E"));
		// 180th Meridian
		LOCATIONS.put("Adak, United States", getPoint("51°53′N	176°38′W"));
		LOCATIONS.put("Nukulaelae, Tuvalu", getPoint("9°23′S	179°51′E"));
		// Just for fun
		LOCATIONS.put("San Francisco", getPoint("37°46′N	122°25′W"));
		LOCATIONS.put("Taipei, Taiwan", getPoint("25°02′N	121°38′E"));
		LOCATIONS.put("Sydney, Australia", getPoint("33°52′S	151°13′E"));
		LOCATIONS.put("Buenos Aires, Argentina", getPoint("34°40′S	58°24′W"));
	}

	public static Map<String,Point> getLocations() {
		return LOCATIONS;
	}

	/**
	 * Testing function to convert from degrees/minutes/seconds to dec degrees.
	 *
	 * @param deg number of degrees
	 * @param min number of minutes
	 * @param sec number of seconds
	 * @param dir directional component; one of: N, E, S, W
	 * @return decimal degrees represented by the given deg/min/sec/dir
	 */
	public static double dmsToDecDeg(final int deg, final int min,
			final int sec, final String dir) {
		final double totalSeconds = min * 60 + sec;
		final double decimalDegrees = totalSeconds / 3600d;
		final double result = (double)deg + decimalDegrees;
		final String direction = dir.toUpperCase(Locale.US);
		if (direction.equals("N") || direction.equals("E")) {
			return result;
		} else if (direction.equals("S") || direction.equals("W")) {
			return -1d * result;
		}
		return 0;
	}

	/**
	 * Testing function to convert a deg/min/sec string to decimal degrees.
	 *
	 * Only for testing; conversion is not even remotely robust.
	 *
	 * @param dmsString string of the format 40°26′47″N
	 * @return decimal degrees represented by the given string
	 */
	public static double dmsToDecDeg(final String dmsString) {
		final String[] degSplit = dmsString.split("°");
		if (degSplit.length == 2) {
			final int deg = Integer.valueOf(degSplit[0]);
			final String[] minSplit = degSplit[1].split("′");
			if (minSplit.length == 2) {
				final int min = Integer.valueOf(minSplit[0]);
				final String[] secSplit = minSplit[1].split("″");
				if (secSplit.length == 2) {
					final int sec = Integer.valueOf(secSplit[0]);
					return dmsToDecDeg(deg, min, sec, secSplit[1].trim());
				} else {
					return dmsToDecDeg(deg, min, 0, minSplit[1].trim());
				}
			} else {
				return deg;
			}
		} else {
			return 0;
		}
	}

	public static Point getPoint(final String tsv) {
		final String[] coords = tsv.split("\t");
		return new Point(dmsToDecDeg(coords[0]), dmsToDecDeg(coords[1]));
	}
}
